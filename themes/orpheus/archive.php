<?php
/**
 * The template for displaying Archive pages.
 *
 * Used to display archive-type pages if nothing more specific matches a query.
 * For example, puts together date-based pages if no date.php file exists.
 *
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * Methods for TimberHelper can be found in the /lib sub-directory
 *
 * @package  WordPress
 * @subpackage  Timber
 * @since   Timber 0.2
 */

 $templates = array( 'archive.twig', 'index.twig' );

 $context = Timber::context();
 $query = array();
 $context['title'] = 'Archive';

 global $paged;
if (!isset($paged) || !$paged){
    $paged = 1;
}


 if ( is_day() ) {
 	$context['title'] = 'Archive: ' . get_the_date( 'D M Y' );
  $query = array(
    "post_type" => array( "post", "video_post", "event", "curated-article", "book", "primary-source", "chapter"),
    "paged" => $paged,
  );
  $context['posts'] = new Timber\PostQuery($query);
 } elseif ( is_month() ) {
 	$context['title'] = 'Archive: ' . get_the_date( 'M Y' );
  $query = array(
    "post_type" => array( "post", "video_post", "event", "curated-article", "book", "primary-source", "chapter"),
    "paged" => $paged,
  );
  $context['posts'] = new Timber\PostQuery($query);
 } elseif ( is_year() ) {
 	$context['title'] = 'Archive: ' . get_the_date( 'Y' );
  $query = array(
    "post_type" => array( "post", "video_post", "event", "curated-article", "book", "primary-source", "chapter"),
    "paged" => $paged,
  );
  $context['posts'] = new Timber\PostQuery($query);
 } elseif ( is_tag() ) {
 	$context['title'] = single_tag_title( '', false );
  $query = array(
    "post_type" => array( "post", "video_post", "event", "curated-article", "book", "primary-source", "chapter"),
    "paged" => $paged,
  );
  $term = get_queried_object();
  $query['tag'] = $term->slug;
  $context['posts'] = new Timber\PostQuery($query);
 } elseif ( is_category() ) {
 	$context['title'] = single_cat_title( '', false );
  $query = array(
    "post_type" => array( "post", "video_post", "event", "curated-article", "book", "primary-source", "chapter"),
    "paged" => $paged,
  );
  $term = get_queried_object();
  $query['category_name'] = $term->slug;
  $context['posts'] = new Timber\PostQuery($query);
 	array_unshift( $templates, 'archive-' . get_query_var( 'cat' ) . '.twig' );
 } elseif ( is_post_type_archive() ) {
 	$context['title'] = post_type_archive_title( '', false );
 	array_unshift( $templates, 'archive-' . get_post_type() . '.twig' );
  $context['posts'] = new Timber\PostQuery();
} else {
  $query = array(
    "post_type" => array( "post", "video_post", "event", "curated-article", "book", "primary-source", "chapter"),
    "paged" => $paged,
  );
  $context['posts'] = new Timber\PostQuery($query);
}


 Timber::render( $templates, $context );
