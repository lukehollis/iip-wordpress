<?php
/**
 * Timber starter-theme
 * https://github.com/timber/starter-theme
 *
 * @package  WordPress
 * @subpackage  Timber
 * @since   Timber 0.1
 */

define( 'DISALLOW_FILE_EDIT' , true );

/**
 * If you are installing Timber as a Composer dependency in your theme, you'll need this block
 * to load your dependencies and initialize Timber. If you are using Timber via the WordPress.org
 * plug-in, you can safely delete this block.
 */
$composer_autoload = __DIR__ . '/vendor/autoload.php';
if ( file_exists( $composer_autoload ) ) {
	require_once $composer_autoload;
	$timber = new Timber\Timber();
}

/**
 * This ensures that Timber is loaded and available as a PHP class.
 * If not, it gives an error message to help direct developers on where to activate
 */
if ( ! class_exists( 'Timber' ) ) {

	add_action(
		'admin_notices',
		function() {
			echo '<div class="error"><p>Timber not activated. Make sure you activate the plugin in <a href="' . esc_url( admin_url( 'plugins.php#timber' ) ) . '">' . esc_url( admin_url( 'plugins.php' ) ) . '</a></p></div>';
		}
	);

	add_filter(
		'template_include',
		function( $template ) {
			return get_stylesheet_directory() . '/static/no-timber.html';
		}
	);
	return;
}

/**
 * Sets the directories (inside your theme) to find .twig files
 */
Timber::$dirname = array( 'templates', 'views' );

/**
 * By default, Timber does NOT autoescape values. Want to enable Twig's autoescape?
 * No prob! Just set this value to true
 */
Timber::$autoescape = false;


/**
 * We're going to configure our theme inside of a subclass of Timber\Site
 * You can move this to its own file and include here via php's include("MySite.php")
 */
class StarterSite extends Timber\Site {
	/** Add timber support. */
	public function __construct() {
		add_action( 'after_setup_theme', array( $this, 'theme_supports' ) );
		add_filter( 'timber/context', array( $this, 'add_to_context' ) );
		add_filter( 'timber/twig', array( $this, 'add_to_twig' ) );
		add_action( 'init', array( $this, 'register_post_types' ) );
		add_action( 'init', array( $this, 'register_taxonomies' ) );
		parent::__construct();
	}
	/** This is where you can register custom post types. */
	public function register_post_types() {
	}
	/** This is where you can register custom taxonomies. */
	public function register_taxonomies() {

	}

	/** This is where you add some context
	 *
	 * @param string $context context['this'] Being the Twig's {{ this }}.
	 */
	public function add_to_context( $context ) {
		$context['is_blog']   = get_option('is_blog');
		$context['is_main_site']   = get_option('is_main_site');
		$context['is_milmanparry']   = get_option('is_milmanparry');
		$context['is_newalexandria']   = get_option('is_newalexandria');
		$context['is_ilexfoundation']   = get_option('is_ilexfoundation');
		$context['show_feedback_chat']   = get_option('show_feedback_chat');
		$context['menu']  = new Timber\Menu("Navigation");
		$context['mega_menu']  = new Timber\Menu("MegaMenu");
		$context['footer_menu']  = new Timber\Menu("FooterNavigation");
		$context['site']  = $this;
		$context['sidebar_posts'] = new Timber\PostQuery(array("posts_per_page" => 3, "post_type" => array("post", "book"), "orderby"  => "rand", ));
		$context['categories'] = Timber::get_terms(array( 'taxonomy' => 'category', 'hide_empty' => false, 'number' => 6, 'orderby' => 'count' ));
		$context['stories_page'] = new Timber\Post("stories-page");
		$context['url_helper'] = new Timber\URLHelper();

		if (get_current_user_id()) {
			$context['saved_books']  = get_user_meta(get_current_user_id(), "saved_books");
			if (count($context['saved_books'])) {
				$context['saved_books'] = new Timber\PostQuery(array("posts_per_page" => -1, "post_type" => "book", 'order' => "ASC", 'orderby' => "title", 'post__in' => $context['saved_books'][0]));
			}
		} else {
			$context['saved_books'] = array();
		}

		function startsWith ($string, $startString)
		{
			$len = strlen($startString);
			return (substr($string, 0, $len) === $startString);
		}

		return $context;
	}

	public function theme_supports() {
		// Add default posts and comments RSS feed links to head.
		add_theme_support( 'automatic-feed-links' );

		/*
		 * Let WordPress manage the document title.
		 * By adding theme support, we declare that this theme does not use a
		 * hard-coded <title> tag in the document head, and expect WordPress to
		 * provide it for us.
		 */
		add_theme_support( 'title-tag' );

		/*
		 * Enable support for Post Thumbnails on posts and pages.
		 *
		 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
		 */
		add_theme_support( 'post-thumbnails' );

		/*
		 * Switch default core markup for search form, comment form, and comments
		 * to output valid HTML5.
		 */
		add_theme_support(
			'html5',
			array(
				'comment-form',
				'comment-list',
				'gallery',
				'caption',
			)
		);

		/*
		 * Enable support for Post Formats.
		 *
		 * See: https://codex.wordpress.org/Post_Formats
		 */
		add_theme_support(
			'post-formats',
			array(
				'aside',
				'image',
				'video',
				'quote',
				'link',
				'gallery',
				'audio',
			)
		);

		add_theme_support( 'menus' );
	}

	/** find the searched term in the string
	 *
	 * @param string $text being searched
	 */
	public function search_body_text( $text, $search_term ) {
		$text  = strip_tags($text);
		$text = html_entity_decode($text);
		$text = str_replace($search_term, "<strong>" . $search_term . "</strong>", $text);

		$pos = strpos($text, $search_term);
		if ($pos && $pos > 120) {
			$text = "&hellip; " . substr($text, $pos - 120, 240) . "	&hellip;";
		} else {
			$text = substr($text, 0, 240) . " &hellip;";
		}

		return $text;
	}

	/** This is where you can add your own functions to twig.
	 *
	 * @param string $twig get extension.
	 */
	public function add_to_twig( $twig ) {
		$twig->addExtension( new Twig\Extension\StringLoaderExtension() );
		$twig->addFilter( new Twig\TwigFilter( 'myfoo', array( $this, 'myfoo' ) ) );
		$twig->addFilter( new Twig\TwigFilter( 'search_body_text', array( $this, 'search_body_text' ) ) );
		return $twig;
	}

}

new StarterSite();


add_filter( 'rp4wp_append_content', '__return_false' );
add_filter( 'rp4wp_disable_css', '__return_true' );



Routes::map('/search/page/:page', function($params){
    $query = '';
    Routes::load('search.php', $params, $query, 200);
});

Routes::map('/search/:query', function($params){
    $query = '';
    Routes::load('search.php', $params, $query, 200);
});
Routes::map('/search', function($params){
    $query = '';
    Routes::load('search.php', $params, $query, 200);
});

Routes::map('/home', function($params){
    $query = '';
    Routes::load('index.php', $params, $query, 200);
});

add_action( 'wp_enqueue_scripts', 'iip_enqueue_scripts' );
function iip_enqueue_scripts() {
    // Enqueue the script which makes the AJAX call to /wp-json/chs/v1/foo.
    wp_enqueue_script('chsSite', '/wp-content/themes/orpheus/static/js/site.js', ['jquery'], '1.1', true);
    wp_enqueue_script('chsSiteReact2', '/wp-content/themes/orpheus/client/build/static/js/2.bundle.js', ['jquery'], '1.1', true);
    wp_enqueue_script('chsSiteReact0', '/wp-content/themes/orpheus/client/build/static/js/0.bundle.js', ['jquery'], '1.1', true);
    wp_enqueue_script('chsSiteReact', '/wp-content/themes/orpheus/client/build/static/js/bundle.js', ['jquery'], '1.1', true);

    // Register custom variables for the AJAX script.
    wp_localize_script( 'chsSite', 'chsScriptVars', [
        'root'  => esc_url_raw( rest_url() ),
        'nonce' => wp_create_nonce( 'wp_rest' ),
    ] );
    wp_localize_script( 'chsSiteReact2', 'chsScriptVars', [
        'root'  => esc_url_raw( rest_url() ),
        'nonce' => wp_create_nonce( 'wp_rest' ),
    ] );
    wp_localize_script( 'chsSiteReact0', 'chsScriptVars', [
        'root'  => esc_url_raw( rest_url() ),
        'nonce' => wp_create_nonce( 'wp_rest' ),
    ] );
    wp_localize_script( 'chsSiteReact', 'chsScriptVars', [
        'root'  => esc_url_raw( rest_url() ),
        'nonce' => wp_create_nonce( 'wp_rest' ),
    ] );
}

add_action( 'after_setup_theme', 'iip_adjust_image_sizes' );
function iip_adjust_image_sizes() {
    add_image_size( 'chs-custom-thumbnail', 420, 360, true );
}


// function create_ACF_meta_in_REST() {
//     $postypes_to_exclude = ['acf-field-group','acf-field'];
//     $extra_postypes_to_include = ["page"];
//     $post_types = array_diff(get_post_types(["_builtin" => false], 'names'), $postypes_to_exclude);
//
//     array_push($post_types, $extra_postypes_to_include);
//
//     foreach ($post_types as $post_type) {
//         register_rest_field( $post_type, 'ACF', [
//             'get_callback'    => 'expose_ACF_fields',
//             'schema'          => null,
//        ]
//      );
//     }
//
// }
//
// function expose_ACF_fields( $object ) {
//     $ID = $object['id'];
//     return get_fields($ID);
// }
//
// add_action( 'rest_api_init', 'create_ACF_meta_in_REST' );
