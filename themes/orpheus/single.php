<?php
/**
 * The Template for displaying all single posts
 *
 * Methods for TimberHelper can be found in the /lib sub-directory
 *
 * @package  WordPress
 * @subpackage  Timber
 * @since    Timber 0.1
 */
global $params;
$context         = Timber::context();
$post     = Timber::query_post($params['id']);
$context['post'] = $post;
$context['page_title'] = $post->title;
$context['attachments'] = array();

$post->meta = get_post_meta($post->ID);
if ($post->meta['attachments']) {
	$post->meta['attachments'] = JSON_decode($post->meta['attachments'][0]);
	$post->meta['attachments'] = $post->meta['attachments']->attachments;

	foreach($post->meta['attachments'] as $attachment) {
		$attachment->url = "/" . $post->category . "/full/" . $attachment->fields->title;

		$file_is_image = false;
		$image_filename_endings = array("jpeg", "jpg", "png", "tiff", "tif");
		foreach ($image_filename_endings as $image_filename_ending) {
			$length = strlen($image_filename_ending);
			if (substr($attachment->fields->title, -$length) === $image_filename_ending){
				$file_is_image = true;
			}
		}
		if ($file_is_image) {
			$context['attachments'][] = $attachment;
		}
	}
}

$context['post']->meta = get_post_meta($post->ID);
$context['annotations'] = new Timber\PostQuery(array(
	"post_type" => "annotation",
	"meta_query" => array(
		array(
			"key" => "on_post",
			"value" => $post->ID,
			"compare" => "=",
		),
		"relation" => "AND",
	),
));
foreach ($context['annotations'] as $annotation) {
	$annotation->author = $annotation->author();
	$annotation->author->avatar = get_avatar_url($annotation->author->ID);
	$annotation->delete_link = get_delete_post_link($annotation->ID);
}

$posts_trending_query = new Timber\PostQuery(array(
	"posts_per_page" => 10,
	"post_type" => array( "post", "story", "resource", "inscription"),
	'meta_key' => 'wpb_post_views_count',
	'orderby' => 'meta_value_num',
	'order' => 'DESC',
  "tax_query" => array(
      array(
          'taxonomy' => 'category',
          'field' => 'slug',
          'terms' => 'news',
          'include_children' => true,
          'operator' => 'NOT IN'
      )
  ),
));

$context['random_posts'] = array();
$trending_posts = array();
foreach($posts_trending_query as $trending_post) {
	$trending_posts[] = $trending_post;
}

$keys = array_rand($trending_posts, 3);
foreach ($keys as $key) {
	$context['random_posts'][] = $trending_posts[$key];
}

$context['user_can_annotate'] = FALSE;
$user = wp_get_current_user();
if ( in_array( 'author', (array) $user->roles ) || in_array( 'administrator', (array) $user->roles )) {
	$context['user_can_annotate'] = TRUE;
}

// track post view
wpb_set_post_views($post->ID);


if ( post_password_required( $context['post']->ID ) ) {
	Timber::render( 'single-password.twig', $context );
} else {
	if ($context['is_blog']) {
		Timber::render( array( 'single-' . $context['post']->ID . '.twig', 'single-' . $context['post']->post_type . '.twig', 'single-' . $context['post']->slug . '.twig', 'single-blog.twig' ), $context );
	} else {
		Timber::render( array( 'single-' . $context['post']->ID . '.twig', 'single-' . $context['post']->post_type . '.twig', 'single-' . $context['post']->slug . '.twig', 'single.twig' ), $context );
	}
}
