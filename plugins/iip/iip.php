<?php

/**
 * @package WordPress
 * @subpackage IIP Wordpress
 *
 * Plugin Name: IIP
 * Description: Plugin associated with the IIP Wordpress website
 * Version: 1.0.0
 * Author: Brown University Library
 */

define('IIP_PLUGIN_DIR', plugin_dir_path( __FILE__ ) );

// require settings
require_once( IIP_PLUGIN_DIR .'config/post_types.php.inc');
require_once( IIP_PLUGIN_DIR .'config/settings.php.inc');
require_once( IIP_PLUGIN_DIR .'config/taxonomies.php.inc');

// require other specific plugin functionalities
require_once( IIP_PLUGIN_DIR .'inc/dashboard.php.inc');
require_once( IIP_PLUGIN_DIR .'inc/meta_keyword_search.php.inc');
require_once( IIP_PLUGIN_DIR .'inc/post_counts.php.inc');
require_once( IIP_PLUGIN_DIR .'inc/shortcodes.php.inc');

add_filter('xmlrpc_enabled', '__return_false');
