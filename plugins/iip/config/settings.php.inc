<?php

/**
 * @package WordPress
 * @subpackage IIP
 *
 * Sundry adjustments and settings for the theme
 */


// Remove select links from the WordPress admin top navigation bar
function iip_remove_admin_bar_links() {
    global $wp_admin_bar;
    //$wp_admin_bar->remove_menu('wp-logo');          // Remove the WordPress logo
    $wp_admin_bar->remove_menu('about');            // Remove the about WordPress link
    $wp_admin_bar->remove_menu('wporg');            // Remove the WordPress.org link
    $wp_admin_bar->remove_menu('documentation');    // Remove the WordPress documentation link
    $wp_admin_bar->remove_menu('support-forums');   // Remove the support forums link
    $wp_admin_bar->remove_menu('feedback');         // Remove the feedback link
    //$wp_admin_bar->remove_menu('site-name');        // Remove the site name menu
    //$wp_admin_bar->remove_menu('view-site');        // Remove the view site link
    //$wp_admin_bar->remove_menu('updates');          // Remove the updates link
    //$wp_admin_bar->remove_menu('comments');         // Remove the comments link
    //$wp_admin_bar->remove_menu('edit');         // Remove the comments link
    //$wp_admin_bar->remove_menu('new-content');      // Remove the content link
    //$wp_admin_bar->remove_menu('w3tc');             // If you use w3 total cache remove the performance link
    //$wp_admin_bar->remove_menu('my-account');       // Remove the user details tab
}
add_action( 'wp_before_admin_bar_render', 'iip_remove_admin_bar_links' );



function iip_gutenberg_css(){

	add_theme_support( 'editor-styles' ); // if you don't add this line, your stylesheet won't be added
	add_editor_style( 'chs-gutenberg.css' ); // tries to include style-editor.css directly from your theme folder

}

add_action( 'after_setup_theme', 'iip_gutenberg_css' );


function my_acf_google_map_api( $api ){
    $api['key'] = 'AIzaSyDHZUm3dFeiYQ773mXMSSkvH9BZ9B00M3s';
    return $api;
}
add_filter('acf/fields/google_map/api', 'my_acf_google_map_api');
