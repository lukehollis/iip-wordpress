<?php
/**
 * Add custom taxonomies
 *
 * Additional custom taxonomies can be defined here
 * http://codex.wordpress.org/Function_Reference/register_taxonomy
 */

function iip_add_custom_taxonomies() {

  register_taxonomy('author', 'post', array(
    // Hierarchical taxonomy (like categories)
    'hierarchical' => true,

    // This array of options controls the labels displayed in the WordPress Admin UI
    'labels' => array(
      'name' => _x( 'Author', 'taxonomy general name' ),
      'singular_name' => _x( 'Author', 'taxonomy singular name' ),
      'search_items' =>  __( 'Search Authors' ),
      'all_items' => __( 'All Authors' ),
      'parent_item' => __( 'Parent Author' ),
      'parent_item_colon' => __( 'Parent Author:' ),
      'edit_item' => __( 'Edit Author' ),
      'update_item' => __( 'Update Author' ),
      'add_new_item' => __( 'Add New Author' ),
      'new_item_name' => __( 'New Author' ),
      'menu_name' => __( 'Authors' ),
    ),

    'show_in_rest' => true,

    // Control the slugs used for this taxonomy
    'rewrite' => array(
      'slug' => 'authors',
      'with_front' => false,
      'hierarchical' => true // This will allow URL's like "/locations/boston/cambridge/"
    ),
  ));

}

add_action( 'init', 'iip_add_custom_taxonomies', 0 );
