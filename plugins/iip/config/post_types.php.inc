<?php
/**
 * @package WordPress
 * @subpackage IIP
 *
 * Register necessary custom post types
 *
 */

add_action( 'init', 'create_post_type' );
function create_post_type() {

  register_post_type( 'story',
    array(
      'labels' => array(
        'name' => __( 'Stories' ),
      ),
    'supports' => array( 'title', 'editor', 'thumbnail', 'comments' ),
    'public' => true,
    'has_archive' => true,
    'description' => 'Story',
    'map_meta_cap' => true,
    'taxonomies' => array( 'category', 'post_tag', 'author' ),
    'exclude_from_search' => false,
    'publicly_queryable' => true,
    'show_in_rest' => true,
    )
  );

  register_post_type( 'resources',
    array(
      'labels' => array(
        'name' => __( 'Educational Resources' ),
      ),
    'supports' => array( 'title', 'editor', 'thumbnail', 'comments' ),
    'public' => true,
    'has_archive' => true,
    'description' => 'Educational Resources',
    'map_meta_cap' => true,
    'taxonomies' => array( 'category', 'post_tag', 'author' ),
    'exclude_from_search' => false,
    'publicly_queryable' => true,
    'show_in_rest' => true,
    )
  );
  register_post_type( 'inscription',
    array(
      'labels' => array(
        'name' => __( 'Inscriptions' ),
      ),
    'supports' => array( 'title', 'editor', 'thumbnail', 'comments' ),
    'public' => true,
    'has_archive' => true,
    'description' => 'Inscription',
    'map_meta_cap' => true,
    'taxonomies' => array( 'category', 'post_tag', 'author' ),
    'exclude_from_search' => false,
    'publicly_queryable' => true,
    'show_in_rest' => true,
    )
  );
}
