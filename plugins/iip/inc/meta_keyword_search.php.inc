<?php
/**
 * @package WordPress
 * @subpackage IIP
 *
 * Meta keyword search
 */

add_action( 'pre_get_posts', function( $q )
{
    if( $s = $q->get( '_meta_or_keyword' ) )
    {
        add_filter( 'get_meta_sql', function( $sql ) use ( $s )
        {
            global $wpdb;

            // Only run once:
            static $nr = 0;
            if( 0 != $nr++ ) return $sql;

            // Modified WHERE
            $sql['where'] = sprintf(
                " AND ( %s OR %s ) ",
                $wpdb->prepare( "{$wpdb->posts}.post_title LIKE '%%%s%%' OR {$wpdb->posts}.post_content LIKE '%%%s%%'", $s, $s),
                mb_substr( $sql['where'], 5, mb_strlen( $sql['where'] ) )
            );

            return $sql;
        });
    }
});
