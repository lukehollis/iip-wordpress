<?php
/**
 * @package WordPress
 * @subpackage IIP
 *
 * Configure dashboard page
 */


function iip_remove_dashboard_widgets()
{
    remove_meta_box('dashboard_site_health', 'dashboard', 'normal');
    remove_meta_box('dashboard_primary', 'dashboard', 'core'); // Remove WordPress Events and News
    remove_meta_box('wpseo-dashboard-overview', 'dashboard', 'normal'); // Remove WordPress Events and News
}

add_action('wp_dashboard_setup', 'iip_remove_dashboard_widgets');

add_action( 'customize_register', 'prefix_remove_css_section', 15 );
/**
 * Remove the additional CSS section, introduced in 4.7, from the Customizer.
 * @param $wp_customize WP_Customize_Manager
 */
function prefix_remove_css_section( $wp_customize ) {
	// $wp_customize->remove_section( 'custom_css' );
  $wp_customize->remove_section("static_front_page");
}
