<?php
/**
 * @package WordPress
 * @subpackage IIP
 *
 * misc shortcodes
 *
 */


function add_break($attrs, $content=""){

  return str_replace("*break*", "<br/>", $content);
}

add_shortcode('add_break', 'add_break');

function highlight($attrs, $content=""){

  return '<span class="highlight" style="background: ' . $attrs['background'] . '; color:' . $attrs['color'] . '">' . $content . '</span>';
}

add_shortcode('highlight', 'highlight');

function speaker($attrs, $content=""){

  return '<label class="labelCaps speaker">' . $content . '</label>';
}

add_shortcode('speaker', 'speaker');

function sup($attrs, $content=""){

  return '<sup>' . $content . '</sup>';
}

add_shortcode('sup', 'sup');

function sub($attrs, $content=""){

  return '<sub>' . $content . '</sub>';
}

add_shortcode('sub', 'sub');
